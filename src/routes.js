import { BrowserRouter, Route, Routes } from "react-router-dom";
import Inicio from "./pages/Home"
import Contact from "./pages/Contact"
import Menu from "./componentes/Menu";
import Footer from "componentes/Footer";
import DefaultPage from "componentes/DefaultPage";
import Works from "pages/Works";
import Tecnologies from "pages/Tecnologies";
import WorkDetail from "pages/WorkDetail";
import NotFound from "pages/NotFound";

function AppRoutes() {
  return (
      <BrowserRouter>
          <Menu/>

          <Routes>
            <Route path="/" element={<DefaultPage/>}>                 {/* Rota Pai  - Em DefaultPage, ele sabe qual pagina carregar graças ao Outlet*/}
              <Route index element={<Inicio/>}/>                      {/* Rota Filha */}
              <Route path="contact" element={<Contact/>}/>            {/* Rota Filha */}
              <Route path="works" element={<Works/>}/>                {/* Rota Filha */}
              <Route path="tecnologies" element={<Tecnologies/>}/>    {/* Rota Filha */}
              
            </Route>

            <Route path="workDetails/:id" element={<WorkDetail/>}/> {/* Rota Filha */}
            <Route path="*" element={<NotFound/>}/>
          </Routes>

          <Footer/>
      </BrowserRouter>
  );
}

export default AppRoutes;
