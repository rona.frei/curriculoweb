import React from 'react';
import styles from './Home.module.css';
import coverPhoto from "assets/capa.png";
import ModelPost from 'componentes/ModelPost';

function Inicio() {

  return (
    <ModelPost coverPhoto={coverPhoto}
               title='Um pouco sobre mim...'>
      <p className={styles.p}>
        Sou desenvolvedor de Sistemas de TI desde 2000. 
        Trabalhei muito tempo em BackEnd com Java, Spring Framework, Oracle e MongoDB.
        Também desenvolvi em FrontEnd com tecnologias React e AngularJs. 
        Já trabalhei com Cloud(Aws, Azure) e servidores com JBoss e Weblogic.
        Neste site há mais detalhes da minha trajetória profissional. 
      </p>
      </ModelPost>
  );
}

export default Inicio;

