import ModelPost from "componentes/ModelPost";
import styles from "./Contact.module.css";
import coverPhoto from "assets/capa.png";
import celular from "../../assets/communications/celular.png";
import email from "../../assets/communications/email.jpg";
import linkedin from "../../assets/communications/linkedin.jpg";

function contato() {
    return (
        <ModelPost coverPhoto={coverPhoto}
                   title='Meus Contatos'>
            <h3 className={styles.subtitle}>
                Você pode entrar em contato comigo por:
            </h3>
            <ul>
                <li>
                    <img  src={celular} alt="figura de celular"/> 
                    <p className={styles.paragraph}>13-9-8186-****</p>
                </li>
                <li>
                    <img  src={email} alt="figura de email"/>
                    <p className={styles.paragraph}>rona.frei@yahoo.com.br</p>
                </li>
                <li>
                    <img  src={linkedin} alt="figura linkedin"/>
                    <p className={styles.paragraph}>https://www.linkedin.com/in/ronafrei/</p>
                </li>
            </ul>

        </ModelPost>
   
    );
}

export default contato;