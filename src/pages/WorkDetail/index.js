import React from "react";
import { Route, Routes, useNavigate, useParams } from "react-router-dom";
import works from "json/experiencias.json";
import styles from "./WorkDetail.module.css";
import NotFound from "pages/NotFound";
import MainButton from "componentes/MainButton";
import DefaultPage from "componentes/DefaultPage";

function WorkDetail() {
  const navegar = useNavigate();
  const parametros = useParams();

  const work = works.find((work) => {
    return work.id === Number(parametros.id);
  });

  if (!work) {
    return (
      <NotFound />
    )
  };


  return (
    <Routes>
      <Route path="*" element={<DefaultPage />}>
        <Route index element={
          <article className={styles.content}>
            <img className={styles.image} src={work.logo} alt={work.empresa} />

            <div className={styles.details}>
              <p className={styles.paragraph}>{work.cargo}</p>
              <p className={styles.paragraph}>{work.localizacao}</p>
              <p className={styles.paragraph}>{work.periodo}</p>
            </div>

            <p className={styles.paragraphDetail}>{work.texto}</p>

            <div onClick={() => navegar(-1)}>
              <MainButton>Voltar</MainButton>
            </div>

          </article>}></Route>
      </Route>
    </Routes>
  );
}

export default WorkDetail;
