import MainButton from 'componentes/MainButton';
import styles from './NotFound.module.css';
import { useNavigate } from 'react-router-dom';

function NotFound(){
    const navegar = useNavigate();

    return(
        <>
            <div className={styles.conteudoContainer}>
                <span className={styles.texto404}>404</span>
                <h1 className={styles.titulo}>
                    Ops! Page Not Found.
                </h1>

                <p className={styles.paragrafo}>
                Are you sure this is what you were looking for? 
                </p>
                <p className={styles.paragrafo}>
                Wait a few moments and reload the page or return to the home page.    
                </p>

                <div className={styles.botaoContainer}
                     onClick={() => navegar("/")}
                >
                    <MainButton tamanho="lg">Voltar</MainButton>
                </div>

            </div>
            <div className={styles.espacoEmBranco} />

        </>
    );
}

export default NotFound;