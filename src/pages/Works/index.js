import styles from "./Works.module.css";

import works from "json/experiencias.json";
import WorkCard from "componentes/WorkCard";

function Works() {
    return (
        <ul className={styles.posts}>
            {works.map((work) => 
                <li key={work.id}>
                    <WorkCard work={work}/>
                </li>
                )}
        </ul>
    );
}

export default Works;