import React from 'react';
import styles from './Tecnologies.module.css';
import Tecnology from 'componentes/Tecnology';
import coverPhoto from "assets/capa.png";
import ModelPost from 'componentes/ModelPost';

function Tecnologies() {
  const tecnologias = Array.from({ length: 19 }, (_, i) => i + 1);

  return (
    <ModelPost coverPhoto={coverPhoto}
               title='Tecnologias Utilizadas'>
      <ul className={styles.ul}>
        {tecnologias.map((i) => (
          <li key={i} className={styles.li}>
            <Tecnology occurs={i} />
          </li>
        ))}
      </ul>
      </ModelPost>
  );
}

export default Tecnologies;

