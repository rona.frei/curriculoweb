import styles from './Banner.module.css'
import logoSmaller from 'assets/logo_menor.png'
import myPhoto from 'assets/minha_foto.png'

function Menu() {
    return (
        <div className={styles.banner}>
            <div>
                <h1 className={styles.title}>
                    Currículo Web
                </h1>
                <p className={styles.paragraph}>
                    Oi! Eu sou o Ronaldo. Full Stack Developer.  
                </p>
            </div>
            <div className={styles.images}>
                <img className={styles.logoSmaller}
                     src={logoSmaller}
                     aria-hidden={true}
                     alt='Logo Thedome'
                />

                <img className={styles.myPhoto}
                     src={myPhoto}
                     alt='Foto Sorrindo'
                />
            </div>
        </div>
    )
}

export default Menu;