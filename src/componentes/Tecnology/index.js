import React from 'react';
import styles from './Tecnology.module.css'; 

function Tecnology({ occurs }) {
  return (
    <div>
      <img
        src={`/assets/ti/${occurs}/imageTecnology.png`}
        alt="Imagens de Tecnologias Utilizadas"
        className={styles.tecnologyImage} 
      />
    </div>
  );
}

export default Tecnology;
