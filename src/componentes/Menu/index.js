import React, { useState } from 'react';
import { FiMenu } from 'react-icons/fi'; // Importe o ícone desejado
import styles from './Menu.module.css';

import MenuLink from '../MenuLink';

export default function Menu() {
    const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);

    const toggleMobileMenu = () => {
        setMobileMenuOpen(!isMobileMenuOpen);
    };

    return (
        <header>
            <nav className={styles.navigation}>
                <div className={styles.mobileMenuIcon} onClick={toggleMobileMenu}>
                    <FiMenu />
                </div>
                <div className={`${styles.menuItems} ${isMobileMenuOpen ? styles.mobileMenuOpen : ''}`}>
                    <MenuLink to="/">Início</MenuLink>
                    <MenuLink to="/works">Experiência Profissional</MenuLink>
                    <MenuLink to="/tecnologies">Tecnologias Conhecidas</MenuLink>
                    <MenuLink to="/contact">Contato</MenuLink>
                </div>
            </nav>
        </header>
    );
}
