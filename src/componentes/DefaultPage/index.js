import { Outlet } from "react-router-dom";
import Banner from "componentes/Banner";

function DefaultPage() {
    return (
        <main>
            <Banner/>

            <Outlet/> {/* Essa tag faz o carregamento das rotas filhas*/}
        </main>    
    );
}

export default DefaultPage;