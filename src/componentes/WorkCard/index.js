import { Link } from "react-router-dom";
import styles from "./Work.module.css";
import MainButton from "componentes/MainButton";

function WorkCard({ work }) {
    return(
        <Link to={`/workDetails/${work.id}`}>
            <div className={styles.work}>
                <img className={styles.cover} 
                     src={`/assets/posts/${work.id}/capa.png`}
                     alt="Imagens de Capas Experiência Profissional"
                />
                <MainButton>Ler</MainButton>
            </div>
        </Link>
        
    );
}

export default WorkCard;