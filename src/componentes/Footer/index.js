import styles from './Footer.module.css'
import { ReactComponent as MarcaRegistrada} from 'assets/marca_registrada.svg'

function Footer() {
    return (
        <div className={styles.footer}>
            <MarcaRegistrada/> Desenvolvido por ThedomeIt
        </div>
    )
}

export default Footer;