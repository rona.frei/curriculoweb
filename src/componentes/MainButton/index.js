import styles from './MainButton.module.css';

function MainButton({ children, tamanho }){
    return(
        <button className={`
                            ${styles.mainButton}
                            ${styles[tamanho]}
                          `}> 
            {children}           
        </button>
    );
}

export default MainButton;